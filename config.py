import os


BASE_PATH = os.path.abspath(os.path.dirname(__file__))


# region SECRET_KEY_GEN
SECRET_FILE = os.path.join(BASE_PATH, "secret.key")
if os.path.exists(SECRET_FILE):
    try:
        with open(SECRET_FILE, 'rt') as fp:
            SECRET_KEY = fp.read().strip()
    except IOError:
        raise Exception("Cannot read secret key file.")
else:
    try:
        import random

        SECRET_KEY = ''.join([random.SystemRandom().choice('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*(-_=+)') for i in range(50)])
        with open(SECRET_FILE, 'wt') as fp:
            fp.write(SECRET_KEY)
    except IOError:
        raise Exception("Cannot write secret key file.")
# endregion

config = {
    "SECRET": SECRET_KEY,
    "ERROR_404_HELP": False
}
