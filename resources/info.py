from datetime import date

from bs4 import BeautifulSoup
from flask_restful import Resource
from requests import Session

from resources.tools import login_required, create_get
from settings.urls import student_urls


def get_profile_info(rsession: Session):
    """
    Retrieves information from the profile view.

    :param rsession: A logged-in session to access the profile page.

    :type rsession: Session

    :return: A dictionary with information.
    :rtype: dict
    """

    r = rsession.get(student_urls["profile"])
    soup = BeautifulSoup(r.text, "lxml")

    name = soup.find_all("span", attrs={"class": "wp3-header"})[0].text.replace('\n', ' ').strip()
    number = soup.find("td", text="\nLeerlingnummer").find_next("td").text.strip()
    school_class = soup.find("td", text="\nKlas").find_next("td").text.strip()
    dob = soup.find("td", text="\nGeboortedatum").find_next("td").text.strip()
    phone_private = soup.find("td", text="\nTelefoon privé").find_next("td").text.strip()
    phone_mobile = soup.find("td", text="\nTelefoon mobiel").find_next("td").text.strip()
    email = soup.find("td", text="\nE-mailadres").find_next("td").text.strip()
    address = soup.find("td", text="\nAdres").find_next("td").text.strip()
    postal_code = soup.find("td", text="\nPostcode").find_next("td").text.strip()
    residence_area = soup.find("td", text="\nWoonplaats").find_next("td").text.strip()
    education_type = soup.find("td", text="\nOnderwijstype").find_next("td").text.strip()

    mentor_name = soup.find("td", text="\nNaam").find_next("td").text.replace('\n', ' ').strip()
    mentor_short = soup.find("td", text="\nPers. code").find_next("td").text.strip()

    return {
        "name": name,
        "number": number,
        "school_class": school_class,
        "dob": dob,
        "phone_private": phone_private,
        "phone_mobile": phone_mobile,
        "email": email,
        "address": address,
        "postal_code": postal_code,
        "residence_area": residence_area,
        "education_type": education_type,
        "mentor": {
            "full_name": mentor_name,
            "short_name": mentor_short
        }
    }


def get_subject_info(rsession: Session):
    """
    Retrieves information from the Subjects view.

    :param rsession: A logged-in session to access the subjects page.

    :type rsession: Session

    :return: A dictionary with information.
    :rtype: dict
    """

    url = create_get(
        wis_ajax=1,
        ajax_object=3211,
        startyear=date.today().year
    )

    r = rsession.get(student_urls["subjects"] + url)
    soup = BeautifulSoup(r.text, "lxml")

    current_year = soup.find("td", text="Leerjaar:").find_next("td").text.strip()
    run_time = soup.find("td", text="Looptijd:").find_next("td").text.replace('\n', ' ').strip()

    subjects = {}
    subject_table_text = {
        "general": "Gemeenschappelijke vakken:",
        "chosen": "Keuzevakken:",
        "education_profile": "Profielvakken:"
    }
    for subject_category in ["general", "chosen", "education_profile"]:
        subject_table = soup.find("td", text=subject_table_text[subject_category]).find_next("td").find("table")

        for element in subject_table.find_all("tr"):
            if subject_category not in subjects.keys():
                subjects[subject_category] = []

            subject = element.td.text.strip()
            subjects[subject_category].append(subject)

    return {
        "current_year": current_year,
        "run_time": run_time,
        "subjects": subjects
    }


def get_classmates(rsession):
    """
    Retrieves information from the Classmates view.

    :param rsession: A logged-in session to access the Classmates page.

    :type rsession: Session

    :return: A dictionary with information.
    :rtype: dict
    """

    url = create_get(
        wis_ajax=1,
        ajax_object=3210,
        view="print"
    )

    r = rsession.get(student_urls["classmates"] + url)
    soup = BeautifulSoup(r.text, "lxml")

    classmates = []
    classmate_list = soup.find("ul", attrs={"class": ["noliststyle", "print-ul"]})
    for element in classmate_list.find_all("li"):
        name = element.find("div", attrs={"class": "wp3-profile-person"}).find("div", attrs={"class": "wp3-header"}).text.replace('\n', " ").strip()
        dob = element.find("div", attrs={"class": "wp3-profile-person"}).find("div", attrs={"class": "wp3-date"}).text.replace('\n', " ").strip()

        classmates.append({
            "name": name,
            "dob": dob
        })

    return classmates


class Info(Resource):
    @login_required
    def get(self, rsession: Session, username, requested_information=None):
        """
        Uses the get_info function to return user information.

        :param requested_information: The information the user wants.
        :param username: The username returned by the wrapper function.
        :param rsession: The session returned by the wrapper function.

        :type requested_information: str
        :type username: str
        :type rsession: Session

        :return: A dictionary with information.
        :rtype: dict
        """

        try:
            if requested_information is None:
                info = get_profile_info(rsession)
                subject_info = get_subject_info(rsession)
                info["current_year"] = subject_info["current_year"]
                info["run_time"] = subject_info["run_time"]
                info["subjects"] = get_subject_info(rsession)
                info["classmates"] = get_classmates(rsession)

                return info

            if requested_information.lower() == "classmates":
                return {
                    "classmates": get_classmates(rsession)
                }
            elif requested_information.lower() == "subjects":
                return {
                    "subjects": get_subject_info(rsession)
                }
            elif requested_information.lower() == "profile":
                return get_profile_info(rsession)
            else:
                return {}
        except Exception as e:
            return {"message": "Information is loading."}, 504
