from resources.authentication import Login
from resources.info import Info
from resources.schedule import Schedule, ScheduleXedule

# The resource map, a dictionary of Resource, List[urls] key pairs.

resource_map = {
    Login: [
        "/login"
    ],
    Info: [
        "/info",
        "/info/<string:requested_information>"
    ],
    Schedule: [
        "/schedule",
        "/schedule/date/<string:start_date>",
    ],
    ScheduleXedule: [
        "/schedule/t/<string:teacher_name>",
        "/schedule/c/<string:classroom>",
        "/schedule/k/<string:cluster>",
    ]
}
