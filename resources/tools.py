import functools

import jwt
import requests
from flask import current_app, request
from flask_restful import abort

from settings.urls import student_urls
from bs4 import BeautifulSoup


def do_login(username, password):
    """
    Perform the login into Quadraam's SSO platform.

    :param username: The username.
    :param password: The password.

    :type username: str
    :type password: str

    :return: A logged-in Session object on success.
    :rtype: Session or None
    """

    payload = {
        "UserName": f"quadraam\\{username}",
        "Password": password
    }

    try:
        s = requests.Session()
        r = s.get(student_urls["profile"])
        r = s.post(r.url, data=payload)
        soup = BeautifulSoup(r.text, "lxml")

        saml_response = soup.find("input", attrs={"name": "SAMLResponse"})["value"]
        post_action = soup.find("form", attrs={"name": "hiddenform"})["action"]

        r = s.post(post_action, data={"SAMLResponse": saml_response})

        if r.url == student_urls["profile"]:
            return s
        else:
            return 401
    except TypeError:
        return 503


def check_login(cookie):
    """
    A function which checks if the login cookie is still valid.

    :param cookie:
    :return: True on logged in.
    """

    s = requests.Session()
    s.cookies.update({
        "JSESSIONID": cookie
    })

    r = s.get(student_urls["schedule_endpoint"])
    if r.status_code == 401:
        return False
    elif r.status_code == 200:
        return True


def create_get(**kwargs):
    """
    Creates a GET request url from kwargs.

    :param kwargs: Dictionary of GET parameters.

    :return: A GET request url.
    :rtype: str
    """

    r = []
    for key in kwargs:
        r.append([key, str(kwargs[key])])

    r = ["=".join(x) for x in r]
    r = "?" + "&".join(r)

    return r


def login_required(method):
    """
    A wrapper function to make sure Resources that need authentication, have it.

    :param method: The function to call when authentication succeeds.

    :return: The function.
    """

    @functools.wraps(method)
    def wrapper(self, *args, **kwargs):
        token = request.headers.get("Authorisation")
        if token is None:
            abort(400, message="No 'Authorisation' header.")

        try:
            token_decoded = jwt.decode(token.encode("utf-8"), current_app.config.get("SECRET"), algorithms="HS256")
            cookie = token_decoded.get("cookie")
            username = token_decoded.get("username")
        except (jwt.DecodeError, jwt.ExpiredSignatureError):
            abort(401, message="Invalid or expired token.")

        if not check_login(cookie):
            abort(401, message="LC cookie has expired.")

        s = requests.Session()
        s.cookies.update({
            "JSESSIONID": cookie
        })

        return method(self, *args, rsession=s, username=username, **kwargs)

    return wrapper
