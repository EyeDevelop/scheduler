import datetime

import jwt
from flask import request, current_app
from flask_restful import Resource
from requests import Session

from resources.tools import do_login, login_required


class Login(Resource):
    def post(self):
        """
        Tries the username and password against Quadraam's servers. If they're correct,
        form a token which can be used to authenticate for all Quadraam activities.

        :return: A JWT token that can be used for authenticating.
        :rtype: str
        """

        username = request.form.get("username")
        password = request.form.get("password")

        if not all([username, password]):
            return {"message": "Please specify both a username and a password."}, 400

        s: Session = do_login(username, password)
        if isinstance(s, int):
            if s == 503:
                return {"message": "The upstream server is down."}, 503
            elif s == 401:
                return {"message": "Invalid credentials."}, 401
        else:
            token = {
                "username": username,
                "cookie": s.cookies.get("JSESSIONID", None, "leerlingen.liemerscollege.nl"),
                "exp": datetime.datetime.utcnow() + datetime.timedelta(hours=3)
            }
            token = jwt.encode(token, current_app.config.get("SECRET"), algorithm="HS256")

            return {"token": token.decode("utf-8")}

    @login_required
    def get(self, rsession=None, username=None):
        """
        Verifies if the user is logged in.

        :return: A 200-response with OK if logged in.
        :rtype: str
        """

        return {"message": "OK"}
