import datetime
import json
import os

import requests
import unidecode
from flask_restful import Resource, abort
from requests import Session

from resources.info import get_profile_info
from resources.tools import login_required, create_get
from resources.xedule import Xedule
from settings.subject_mapper import get_subject
from settings.urls import student_urls


def get_period(time):
    """
    Maps a time to a school period.

    :param time: A string of time.

    :type time: str

    :return: The school period.
    :rtype: int
    """

    if len(time.split(":")) > 2:
        time = ":".join(time.split(":")[:2])
    h, m = map(int, time.split(":"))
    time = h * 60 + m
    time -= 490  # Subtract 8:10 so only a base time remains.

    return time // 25  # Periods are multiples of 25 minutes per hour.


def format_xedule_schedule(schedule, id_classrooms, id_clusters, id_teachers):
    schedule_new = {}
    for lesson in schedule[0]["apps"]:
        y, m, d = map(int, lesson["iStart"].split("T")[0].split("-"))
        weekday = datetime.date(y, m, d).weekday()
        start_period = lesson["iStart"].split("T")[1]
        start_period = get_period(start_period)

        end_period = lesson["iEnd"].split("T")[1]
        end_period = get_period(end_period)

        attendees = lesson["atts"]
        classroom = ""
        clusters = []
        teachers = []

        for i in attendees:
            i = str(i).lower()
            if i in id_teachers.keys():
                teachers.append(id_teachers[i])
            elif i in id_clusters.keys():
                clusters.append(id_clusters[i])
            elif i in id_classrooms.keys():
                classroom = id_classrooms[i]

        if weekday not in schedule_new:
            schedule_new[weekday] = {}

        for i in range(start_period, end_period):
            schedule_new[weekday][i] = {
                "subject": lesson["name"],
                "cluster": clusters,
                "classroom": classroom,
                "teacher": teachers,
            }

    return schedule_new


class Schedule(Resource):
    @login_required
    def get(self, rsession: Session, username=None, start_date=None):
        """
        Retrieves the schedule from the Quadraam API endpoint.

        :param rsession: The session returned by the wrapper function.
        :param username: The username returned by the wrapper function.
        :param start_date: The monday-of-the-week you want to view the schedule for.

        :type rsession: Session
        :type username: str
        :type start_date: str

        :return: The schedule as a JSON-encoded string.
        :rtype: str
        """

        # Check if start_date is valid
        if not start_date:
            today = datetime.date.today()

            # If it's Saturday, get the schedule for next week.
            if today.weekday() > 4:
                today += datetime.timedelta(weeks=1)

            # Get monday as start_date.
            start_date = today - datetime.timedelta(days=today.weekday())
        else:
            try:
                start_date = datetime.date(*map(int, start_date.split("-")))
            except (TypeError, ValueError):
                abort(400, message="Invalid date. Should be YYYY-MM-DD")

        # The API only accepts epoch timestamps for some reason, so conversion is needed.
        start = int(datetime.datetime(start_date.year, start_date.month, start_date.day, 0, 0, 0).timestamp() * 1000)

        # Create the GET url with the parameters.
        url = create_get(
            format="json",
            method="getLeerlingRooster",
            start=start,
            so_id=3217
        )

        r = rsession.get(student_urls["schedule_endpoint"] + url)

        # Check if the request was performed successfully.
        if str(r.status_code)[0] == "5":
            return {"message": "Upstream server is having difficulties."}, 502

        # Replace unicode characters with their ASCII counterparts.
        r = json.loads(unidecode.unidecode(r.text))

        if "events" not in r.keys():
            return {"message": "Server is generating schedule, please try again later."}, 504

        # The generation of the actual schedule.
        # The top-level is the weekday as a number from 0-4,
        # then a school period from 0-19, followed by the lesson information.
        mentor_short = get_profile_info(rsession)["mentor"]["short_name"]
        schedule = {}
        for event in r["events"]:
            y, m, d = map(int, event["start"].split("T")[0].split("-"))
            day = datetime.date(y, m, d).weekday()

            cluster = event["afspraakObject"]["lesgroep"]

            # Try to get a subject from a cluster.
            subject = get_subject(cluster, mentor_short)

            teacher_full_name = []
            teacher_short_name = []

            for teacher in event["afspraakObject"]["docent"]:
                teacher_full_name.append(
                    "{} {}".format(teacher["title"], teacher["achternaam"])
                )

                teacher_short_name.append(teacher["afkorting"].lower())

            classroom = event["afspraakObject"]["lokaal"]

            if day not in schedule.keys():
                schedule[day] = {}

            start_time = get_period(event["afspraakObject"]["tijden"].split(" - ")[0])
            end_time = get_period(event["afspraakObject"]["tijden"].split(" - ")[1])

            for i in range(start_time, end_time):
                schedule[day][i] = {
                    "subject": subject,
                    "cluster": cluster.lower(),
                    "classroom": classroom.lower(),
                    "teacher_full": teacher_full_name,
                    "teacher": teacher_short_name
                }

        return schedule


class ScheduleXedule(Resource):
    def get(self, teacher_name=None, classroom=None, cluster=None, week=None, year=None):
        """
        Gets the teacher's schedule from Xedule.

        :param teacher_name: The abbreviation of the teacher.
        :param classroom: The classroom name.
        :param cluster: The cluster name.
        :param week: The week number to get the schedule of.
        :param year: The year of which to get the schedule of.

        :type teacher_name: str
        :type classroom: str
        :type cluster: str
        :type week: str
        :type year: str

        :return: The teachers schedule as a JSON-encoded string.
        :rtype: str
        """

        if not any([teacher_name, classroom, cluster]):
            return {"message": "Missing arguments."}, 400

        xedule = Xedule()

        if not week:
            week = datetime.date.today().isocalendar()[1]
            if datetime.date.today().weekday() > 4:
                week += 1
        if not year:
            year = datetime.date.today().year

        # Make dictionaries for id:name and name:id mapping.
        teachers = xedule.get_teachers()
        teachers_r = dict((y, x) for x, y in teachers.items())

        clusters = xedule.get_clusters()
        clusters_r = dict((y, x) for x, y in clusters.items())

        classrooms = xedule.get_classrooms()
        classrooms_r = dict((y, x) for x, y in classrooms.items())

        attendee_id = None
        if teacher_name:
            if teacher_name in teachers.keys():
                attendee_id = teachers[teacher_name]

        if classroom:
            if classroom in classrooms.keys():
                attendee_id = classrooms[classroom]

        if cluster:
            if cluster in clusters.keys():
                attendee_id = clusters[cluster]

        if attendee_id is None:
            return {"message": "Did not find requested schedule."}, 404

        schedule_raw = xedule.get_schedule(year, week, attendee_id)
        if not schedule_raw:
            return {"message": "Received odd response from Xedule."}, 503

        schedule = format_xedule_schedule(schedule_raw, classrooms_r, clusters_r, teachers_r)

        return schedule
