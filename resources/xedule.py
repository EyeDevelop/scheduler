import json
import os

from requests import Session


class Xedule:
    def __init__(self):
        self.rsession = Session()

        # Get a cookie so requests can be made.
        self.rsession.get("https://sa-liemers.xedule.nl/")

    def get_teachers(self, use_cache: bool = True):
        """
        Gets all the teachers on the Liemers College.

        :param use_cache: Whether to use the cached teachers json.

        :return: A dictionary with teacher_name:id mappings.
        """

        if not os.path.exists("teachers.json") or not use_cache:
            url = "https://sa-liemers.xedule.nl/api/docent/"

            teachers = {}
            teachers_raw = self.rsession.get(url).json()
            for teacher in teachers_raw:
                teachers[teacher["code"].lower()] = teacher["id"]

            with open("teachers.json", "wt") as fp:
                json.dump(teachers, fp)

            return teachers
        else:
            with open("teachers.json", "rt") as fp:
                teachers = json.load(fp)

            return teachers

    def get_clusters(self, use_cache: bool =True):
        """
        Gets all the clusters on the Liemers College.

        :param use_cache: Whether to use the cached teachers json.

        :return: A dictionary with teacher_name:id mappings.
        """

        if not os.path.exists("classes.json") or not use_cache:
            url = "https://sa-liemers.xedule.nl/api/group/"

            classes = {}
            classes_raw = self.rsession.get(url).json()
            for sclass in classes_raw:
                classes[sclass["code"].lower()] = sclass["id"]

            with open("classes.json", "wt") as fp:
                json.dump(classes, fp)

            return classes
        else:
            with open("classes.json", "rt") as fp:
                classes = json.load(fp)

            return classes

    def get_classrooms(self, use_cache: bool =True):
        """
        Gets all the classrooms on the Liemers College.

        :param use_cache: Whether to use the cached teachers json.

        :return: A dictionary with teacher_name:id mappings.
        """

        if not os.path.exists("classrooms.json") or not use_cache:
            url = "https://sa-liemers.xedule.nl/api/facility/"

            classrooms = {}
            classrooms_raw = self.rsession.get(url).json()
            for classroom in classrooms_raw:
                classrooms[classroom["code"].lower()] = classroom["id"]

            with open("classrooms.json", "wt") as fp:
                json.dump(classrooms, fp)

            return classrooms
        else:
            with open("classrooms.json", "rt") as fp:
                classrooms = json.load(fp)

            return classrooms

    def get_schedule(self, year: str, week: str, attendee_id: str):
        """
        Get a schedule for attendee_id (which can be either a teacher_id, classroom_id or a cluster_id).

        :param year: The year to get the schedule for.
        :param week: The week to get the schedule for.
        :param attendee_id: The attendee_id.

        :return: A dictionary with the requested schedule, or None upon failure.
        """

        url = "https://sa-liemers.xedule.nl/api/schedule/?ids[0]={}_{}_{}_{}".format(
            1,  # Organisational unit (1 for LC)
            year,  # The current year
            week,  # The week of the schedule to display.
            attendee_id,  # The ID of the attendee (could be a teacher, a classroom or a cluster).
        )

        r = self.rsession.get(url)

        if r.status_code == 200:
            return r.json()
        else:
            return None
