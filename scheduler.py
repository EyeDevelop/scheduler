from flask import Flask
from flask_restful import Api

from config import config
from database.manager import initialise_database
from resources import resource_map

app = Flask(__name__)
api = Api(app)

# Update the config
app.config.update(config)

# Make sure URL endpoints are set
for k in resource_map.keys():
    api.add_resource(k, *resource_map[k])


# Database has to be initialised before use.
@app.before_first_request
def initialise():
    initialise_database()


# Actually run the Flask server.
if __name__ == "__main__":
    app.run()
