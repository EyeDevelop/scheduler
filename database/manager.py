from database import db

tables = []


def initialise_database():
    """
    Sets the database up for use.

    :return: Nothing
    :rtype: Nothing
    """

    db.connect()
    db_fine = True
    for table in tables:
        if not db.table_exists(table):
            db_fine = False

    if not db_fine:
        db.create_tables(tables)

    print("Initialised database...")
