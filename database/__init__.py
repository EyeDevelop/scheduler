import peewee

db = peewee.SqliteDatabase("scheduler.db")


class BaseModel(peewee.Model):
    class Meta:
        database = db
