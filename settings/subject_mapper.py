subject_map = {
    # Languages
    "cam": "Cambridge Engels",
    "dutl du": "Duits",
    "entl en": "Engels",
    "fatl fa": "Frans",
    "gtc gr": "Grieks",
    "ltc la": "Latijn",
    "netl ne": "Nederlands",
    "s-e": "Spaans",

    # Scientific subjects
    "biol bz": "Biologie",
    "nat na": "Natuurkunde",
    "nc": "Nasktech",
    "nlt": "Natuur, Leven, Technologie",
    "schk sk": "Scheikunde",

    # Communal subjects
    "ak": "Aardrijkskunde",
    "fi": "Filosofie",
    "ges gs": "Geschiedenis",
    "lv": "Levensbeschouwing",
    "maat": "Maatschappijleer",

    # Maths
    "wi": "Wiskunde",
    "wisa": "Wiskunde A",
    "wisb": "Wiskunde B",
    "wisc": "Wiskunde C",
    "wisd": "Wiskunde D",

    # Creative subjects
    "ckv": "Culturele en kunstzinnige vorming",
    "ha": "Handvaardigheid",
    "mu": "Muziek",
    "te": "Tekenen",

    # Financial subjects
    "econ ec": "Economie",
    "m&o": "Management en Organisatie",

    # Obligatory subjects
    "lo": "Gym",
    "men": "Mentoruur",
}


def get_subject(cluster: str, mentor_short: str):
    """
    Tries to map a cluster name to a subject.

    :param mentor_short: The abbreviated name of the mentor.
    :param cluster: The cluster the subject name needs to be guessed from.

    :return: The subject on success.
    """

    cluster = cluster.lower()

    for k in subject_map:
        if any([x in cluster for x in k.split()]):
            return subject_map[k]

        if "{}".format(mentor_short) in cluster:
            return subject_map["men"]

    return None
