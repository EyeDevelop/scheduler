# Provides the URLs needed by this API.

student_urls = {
    "profile": "https://leerlingen.liemerscollege.nl/Portaal/Mijn_info/Persoonlijk/Profiel",
    "schedule_endpoint": "https://leerlingen.liemerscollege.nl/fs/SOMTools/Comps/Agenda.cfc",
    "subjects": "https://leerlingen.liemerscollege.nl/Portaal/Mijn_info/Persoonlijk/Vakkenpakket",
    "classmates": "https://leerlingen.liemerscollege.nl/Portaal/Mijn_info/Persoonlijk/Klassenlijst"
}
